﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

type Matrix(data: float[,]) =
    member x.Data = data
    member x.N with get() = Array2D.length1 x.Data
    member x.M with get() = Array2D.length2 x.Data
    
    member x.Rows : Vector seq = seq { for i in 0..x.N-1 do yield Vector.fromSeq x.Data.[i,0..] }
    member x.Cols : Vector seq = seq { for j in 0..x.M-1 do yield Vector.fromSeq x.Data.[0..,j] }

    member x.Transposed = Matrix(Array2D.init x.M x.N (fun i j -> x.Data.[j, i]))

    /// Multiply i-th row by alpha
    member x.MultRow (i: int, alpha: float) : Matrix =
        let n = x.N
        let e1 = Vector.E n i
        let e2 = (Vector.E n i).Transposed
        let E = (Matrix.I n) + (alpha - 1.0) * e1 * e2
        E * x

    /// Add alpha * i-th row to j-th row
    member x.AddRow (j: int, i: int, alpha: float) = 
        let n = x.N
        let E = Matrix.I(n) + alpha * (Vector.E n i) * (Vector.E n j).Transposed
        E * x

    /// Swap i-th and j-th rows
    member x.SwapRows (i: int, j: int) : Matrix =
        let n = x.N
        let e1: Matrix = (Vector.E n j) - (Vector.E n i)
        let e2: Matrix = ((Vector.E n i) - (Vector.E n j)).Transposed
        let E = (Matrix.I n) + e1 * e2
        E * x

    member x.REF : Matrix =
        Seq.fold (fun m ij -> // moving across the diagonal
                     Seq.fold (fun m1 i ->
                                    let alpha = m1.Data.[i, ij] / m1.Data.[ij, ij]
                                    let res = m1.AddRow(ij, i, -1.0 * alpha)
                                    printfn "added rows -%f * %d + %d, got\n%A\n\n" alpha ij i res
                                    res)
                               m [ij+1..x.N-1]) x [0..x.M-1]

    override x.ToString() = String.concat "\n" <| Seq.map (fun x -> x.ToString()) x.Rows

    static member (*) (alpha: float, m: Matrix) =
        Matrix(Array2D.init m.N m.M (fun i j -> alpha * m.Data.[i,j]))

    static member (*) (m1: Matrix, m2: Matrix) =
        if m1.N <> m2.M then invalidArg "m2" <| sprintf "Matrix dimensions don't match, %d x %d  vs  %d x %d" m1.N m1.M m2.N m2.M

        let vs1 = Seq.toArray m1.Rows
        let vs2 = Seq.toArray m2.Cols
        Matrix(Array2D.init m1.N m2.M (fun i j -> vs1.[i].ScalarMult(vs2.[j])))

    static member (+) (m1: Matrix, m2: Matrix) =
        if m1.N <> m2.N || m1.M <> m2.M then invalidArg "m2" <| sprintf "Matrix dimensions don't match, %d x %d  vs  %d x %d" m1.N m1.M m2.N m2.M
        Matrix(Array2D.init m1.N m1.M (fun i j -> m1.Data.[i,j] + m2.Data.[i,j]))

    static member (-) (m1: Matrix, m2: Matrix) =
        if m1.N <> m2.N || m1.M <> m2.M then invalidArg "m2" <| sprintf "Matrix dimensions don't match, %d x %d  vs  %d x %d" m1.N m1.M m2.N m2.M
        Matrix(Array2D.init m1.N m1.M (fun i j -> m1.Data.[i,j] - m2.Data.[i,j]))

    static member I (n: int) : Matrix = Matrix(Array2D.init n n (fun i j -> if i = j then 1.0 else 0.0))
    static member Z (n: int) : Matrix = Matrix(Array2D.init n n (fun _ _ -> 0.0))
    static member constant (n: int) (x : float) : Matrix = Matrix(Array2D.init n n (fun _ _ -> x))
    static member init (n: int) (f : int -> int -> float) : Matrix = Matrix(Array2D.init n n f)


and Vector(data: float[]) =

    inherit Matrix(Array2D.init data.Length 1 (fun i _ -> data.[i]))

    member x.Data = data
    member x.Length with get() = x.Data.Length

    member x.ScalarMult(other: Vector) =
        Seq.fold (fun sum (x,y) -> x * y + sum) 0.0 <| Seq.zip x.Data other.Data

    override x.ToString() = String.concat " " <| Seq.map (fun x -> x.ToString()) x.Data

    static member E (n: int) (i:int) : Vector = Vector.fromSeq <| Seq.map (fun x -> if x = i then 1.0 else 0.0) [0..n-1]
    static member fromSeq(s: float seq) = Vector([| for i in s do yield i |])

    /// Scalar vector product
    static member (<|>) (v1: Vector, v2: Vector) = v1.ScalarMult(v2)

[<EntryPoint>]
let main argv = 
    let v1 = Vector.fromSeq [1.0 ; 2.0]
    let v2 = Vector.fromSeq [4.0 ; 3.0]

    printfn "%A\n" <| (Matrix(array2D [ [1.0; 0.0; 1.0]; [2.0; 1.0; 0.0]; [0.0; 1.1; 1.0] ])).REF
//    printfn "%A\n" <| (Matrix.init 5 (fun i j -> 1.0 + float (j - i * i) + float (j - 3 + 5 *i))).REF
//    printfn "%A\n" <| (Matrix.I 5).MultRow(3, 3.0).MultRow(2, 2.0).AddRow(1, 2, 1.0).REF

//    let m1 = Matrix(array2D [| [| 1.0; 2.0 |]; [| 3.0; 4.0 |] |])

//    printfn "%A\n\n" <| (m1 <*> (I 2))
//
//    printfn "%A\n" m1
//    Seq.iter (fun row -> printfn "%A" row) m1.Rows
//    printfn "\n"
//    Seq.iter (fun col -> printfn "%A" col) m1.Cols
//    printfn "\n"
//
//    printfn "< %A | %A > = %f" v1 v2 (v1 <|> v2)
//    printfn "%A" <| I 2

    0
